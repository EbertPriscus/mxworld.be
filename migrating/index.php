<?php
/**
 * Created by PhpStorm.
 * User: landervanderstraeten-sintobin
 * Date: 1/02/18
 * Time: 13:14
 * Migrating script to create articles
 */

require($_SERVER['DOCUMENT_ROOT'].'/wp-load.php');

$servername = "mysql219.hosting.combell.com";
$username = "ID231804_mxworld2018";
$password = "IUZet1cH";

// Create connection
$conn = mysqli_connect($servername, $username, $password, $username);
mysqli_set_charset($conn, 'utf8');

// Check connection
if (mysqli_connect_errno()) {
    die("Connection failed: " . $conn->connect_error);
}

if(isset($_GET['start'])) $start = $_GET['start'];
else $start = 0;

$step = 1000;

$sql = "SELECT * FROM tbl_articles ORDER BY id DESC LIMIT $start,$step";
$res = mysqli_query($conn, $sql) or die(mysqli_error($conn));

//mapping voor cats
// pro, amater, foto, contact, laatste nieuws 0 , us news 6
$mapping = array(1 => 5, 2 => 4, 3 => 3, 4 => 1, 0 => 1, 6 => 6);


// Loop trough articles
while($row = mysqli_fetch_assoc($res)){

    $arr['post_author'] = 1;
    $arr['post_status'] = "publish";
    $arr['comment_status'] = "closed";
    $arr['ping_status'] = "closed";
    $arr['post_name'] = mysqli_real_escape_string($conn, strtolower(stripslashes($row['title'])));
    $arr['guid'] = 'http://mxworldbe.webhosting.be/' . $row['id'] . '/' . $arr['post_name'];
    $arr['post_date'] = date('Y-m-d H:i:s', $row['date']);
    $arr['post_date_gmt'] = date('Y-m-d H:i:s', $row['date'] - 1*60*60);
    $arr['post_title'] = $row['title'];
    $arr['post_content'] = strip(html_entity_decode($row['intro'])).'<!--more-->'.strip(html_entity_decode($row['text']));

    // escape
    $arr['post_title'] = mysqli_real_escape_string($conn, $arr['post_title']);
    $arr['post_content'] = mysqli_real_escape_string($conn, $arr['post_content']);

    // collect and add gallery
    $images = null;
    $sql = "SELECT new_id FROM tbl_articles_thumbs_xref AS a JOIN tbl_images AS b ON a.image_id = b.id WHERE article_id='".$row['id']."'";
    $query = mysqli_query($conn, $sql);
    while($r = mysqli_fetch_assoc($query)){
        $images[] = $r['new_id'];
    }
    if(isset($images)) $arr['post_content'] .= '\r\n[gallery ids="'.implode(',', $images).'"]';


    // create post
    $sql = "INSERT IGNORE wp_posts (". implode(',', array_keys($arr)).") VALUES ('". implode("','", array_values($arr))."')";
    mysqli_query($conn, $sql) or die (mysqli_error($conn));
    $id = mysqli_insert_id($conn);

    //Create categories
    /*
    $sql = "SELECT * FROM tbl_articles_cat_xref WHERE article_id='$id' AND cat_id != '0'";
    $query = mysqli_query($conn, $sql);
    $b = false;
    while($r = mysqli_fetch_assoc($query)){
        $b = true;
        $nid = $mapping[$r['cat_id']];
        $sql = "INSERT IGNORE INTO wp_term_relationships (object_id, term_taxonomy_id, term_order) VALUES ('$id', '$nid', 0)";
        mysqli_query($conn, $sql) or die (mysqli_error($conn));
    }

    // set as uncategorized
    if(!$b){
        $sql = "INSERT IGNORE INTO wp_term_relationships (object_id, term_taxonomy_id, term_order) VALUES ('$id', 1, 0)";
        mysqli_query($conn, $sql) or die (mysqli_error($conn));
    }
    */

    // Remember old Id
    $sql = "UPDATE tbl_articles SET new_id = '$id' WHERE id='".$row['id']."'";
    mysqli_query($conn, $sql) or die (mysqli_error($conn));

    // Set random image as thumbnail
    if(isset($images) && count($images) > 0){
        $rnd = rand(0, count($images) - 1);
        set_post_thumbnail( $id, $images[$rnd]);
    }


}

function strip($str){
    return strip_tags($str, '<a><br><br/><img><iframe>');
}

$end = $start + $step;
print '<meta http-equiv="refresh" content="1;url=index.php?start='.$end.'">';
echo "<h1>Ready with start $start  till  $end </h1>";
?>