<div class="articles">
<?php
$i = 0;
if ( have_posts() ) : while ( have_posts() ) : the_post();

?>
    <div class="row">
        <div class="col-sm-12">
            <div class="row d-flex article">
                <div class="col-md-4 col-no-pad thumb-container">
                    <?php if(!has_post_thumbnail()) : ?>
                        <img src="/wp-content/themes/mxworld/htmlassets/no-thumbnail.png">
                    <?php else : ?>
                        <? echo the_post_thumbnail(); ?>
                    <?php endif; ?>
                </div>
                <?php
                // Find out the category
                $collection = [];
                $cats = get_the_category();
                foreach($cats as $cat){
                    $collection[] = $cat->name;
                }
                ?>
                <div class="col-md-8 content">
                    <div class="type"><?php echo implode(' | ', $collection); ?></div>
                    <h3><?php the_title();  ?></h3>
                    <p class="author"><i><?php the_date(); echo ' '; the_time(); ?> - geplaatst door <?php the_author_meta('user_login'); ?> | <span class="fb-comments-count" data-href="http://www.mxworld.be/article/<? echo $post->ID; ?>">0</span> reactie(s)</i></p>
                    <div class="post-text">
                        <p>
                        <?php
                        $pieces = get_extended($post->post_content);
                        echo nl2br(strip_shortcodes($pieces['main'])); //Show intro only
                        ?>
                        </p>
                    </div>
                    <a href="<?php the_permalink(); ?>" class="thor-it">
                        <img src="/wp-content/themes/mxworld/htmlassets/thor-it.gif">
                    </a>
                </div>
            </div>
        </div>

    </div>

    <?php
    // show ads
    if($i === 3){
        echo adrotate_group(105);
        echo adrotate_group(106);
    }
    if($i === 6){
        echo adrotate_group(110);
        echo adrotate_group(111);
    }
    if($i === 9){
        echo adrotate_group(115);
        echo adrotate_group(116);
    }
    if($i === 12){
        echo adrotate_group(120);
        echo adrotate_group(121);
    }
    if($i === 15){
        echo adrotate_group(125);
        echo adrotate_group(126);
    }
    if($i === 18){
        echo adrotate_group(130);
        echo adrotate_group(131);
    }
    ?>

<?php
$i++;
endwhile;
?>
</div>

<div class="row">
    <div class="links justify-content-center d-flex flex-row">
        <div class="nav-next alignright"><?php previous_posts_link( '<i class="fa fa-angle-double-left"></i> Nieuwere posts' ); ?></div><div class="pages justify-content-between">
            <?php wpbeginner_numeric_posts_nav(); ?>
        </div><div class="nav-previous alignleft"><?php next_posts_link( 'Vorige posts <i class="fa fa-angle-double-right"></i>' ); ?></i></div>
    </div>
</div>
<?php else : ?>

    <?php _e('Sorry, no posts matched your criteria.'); ?>

<?php endif; ?>
