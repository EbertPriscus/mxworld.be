<?php get_header(); ?>

<?php
/* Carousel with images */
get_template_part( 'carousel', 'index' );
?>


<?php
/* Advertising */
echo adrotate_group(100);
echo adrotate_group(101);
?>


<?php
/* Get list with articles, including pagination */
get_template_part( 'articles', get_post_format());
?>

<?php get_footer(); ?>