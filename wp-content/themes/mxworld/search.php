<?php get_header(); ?>
    <div class="row">
        <div class="col-sm-12">
            <? if (have_posts()) { ?>
                <h2 class="gallery-title"><?php printf( __( 'Zoekresultaten voor: %s', 'mxworld' ), '<span>' . get_search_query() . '</span>' ); ?></h2>
                <?
                print_r(get_query_var('s'));
                get_template_part( 'articles', get_post_format());
                ?>
            <? } else { ?>
                <h2 class="gallery-title">Geen zoekresultaten</h2>
            <? } ?>
        </div>
    </div>
<?php get_footer(); ?>