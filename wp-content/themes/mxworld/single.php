<?php
/*
 * Template Name: Featured Article
 * Template Post Type: post, page, product
 */

get_header();

if (have_posts()) : while (have_posts()) : the_post();
endwhile;

    ?>


        <div class="row">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner" role="listbox">
                    <div class="carousel-item active">
                        <?
                        $url = has_post_thumbnail() ? get_the_post_thumbnail_url() : '/wp-content/themes/mxworld/htmlassets/no-banner.png';
                        ?>
                        <img src="<? echo $url; ?>" alt="First slide">
                        <div class="carousel-caption big-article d-xs-block text-center">
                            <h2><?php the_title(); ?></h2>
                            <p><?php echo $post->Intro; ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <div class="whiterows">
        <?php
        /* Advertising */
        echo adrotate_group(200);
        ?>

        <div class="row">
            <div class="col-sm-12 big-article-content">
                <p class="text-sm-right"><i><?php the_date();
                        echo ' ';
                        the_time(); ?> - geplaatst door <?php the_author(); ?></i></p>
                <p class="intro"><?php echo $post->Intro; ?></p>
                <div class="text">
                    <?php
                    $pieces = get_extended($post->post_content);
                    echo nl2br(strip_shortcodes($pieces['main']));
                    echo '<br/>';
                    echo nl2br(strip_shortcodes($pieces['extended']));
                    ?>
                </div>
            </div>
        </div>

        <?php
        /* Advertising */
        echo adrotate_group(250);
        ?>


        <?php

        // Find out all images of post

        $post_content = $post->post_content;
        preg_match('/\[gallery.*ids=.(.*).\]/', $post_content, $ids);

        if (isset($ids) && isset($ids[1])) {
            ?>
            <div class="row">
                <div class="col-sm-12 big-article-content" id="lightgallery">
                    <?
                    $images_id = explode(",", $ids[1]); //we get the list of IDs of the gallery as an Array


                    foreach ($images_id as $image) {

                        // Find out thumbnail $postwithbreaks = wpautop( $getPost, true/false );
                        $thumb = wp_get_attachment_image_src($image);

                        // Find out caption and title
                        $original = get_post($image);
                        ?>
                        <a class="thumbnail" href="<? echo $original->guid; ?>" data-src="<? echo $original->guid; ?>"
                           data-sub-html=".caption">
                            <img src="<? echo $thumb[0]; ?>" class="w-100">
                            <div class="caption" style="display:none;">
                                <h3><? echo $original->post_excerpt; ?></h3>
                                <h4><? echo $original->post_content; ?></h4>
                            </div>
                        </a>
                        <?
                    }
                    ?>
                </div>
            </div>
            <?
        }
        ?>

        <div class="row">
            <div class="col-12">
                <div class="fb-comments" data-href="http://www.mxworld.be/article/<? echo $post->ID; ?>" data-num-posts="5" data-width="100%"></div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="links">
            <?php previous_post_link('%link', '<i class="fas fa-angle-double-left"></i> %title'); ?><a
                    href="<?php echo get_bloginfo('wpurl'); ?>"> <i class="fas fa-home"></i>
            </a><?php next_post_link('%link', '%title <i class="fas fa-angle-double-right"></i>'); ?>
        </div>

    </div>

<?php else : ?>

    <?php _e('Sorry, no posts matched your criteria.'); ?>

<?php endif; ?>

<?php get_footer(); ?>
