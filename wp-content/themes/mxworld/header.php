<!doctype html>
<html lang="nl-BE">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo get_bloginfo('template_directory'); ?>/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo get_bloginfo('template_directory'); ?>/style.css">

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js"
            integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4"
            crossorigin="anonymous"></script>

    <!-- Font Awesome -->
    <script defer src="<?php echo get_bloginfo('template_directory'); ?>/font-awesome/svg-with-js/js/fontawesome-all.js"></script>

    <!-- google analytics -->
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-1218579-1']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

    </script>

    <!-- Clamping -->
    <script src="<?php echo get_bloginfo('template_directory'); ?>/3rd/lineclamp/lineclamp.js"></script>

    <!-- Lightgallery -->
    <script src="<?php echo get_bloginfo('template_directory'); ?>/3rd/lightgallery/js/lightgallery-all.js"></script>
    <link rel="stylesheet" href="<?php echo get_bloginfo('template_directory'); ?>/3rd/lightgallery/css/lightgallery.css">

    <!-- Mansonry -->
    <script src="<?php echo get_bloginfo('template_directory'); ?>/3rd/mansonry/mansonry.js"></script>


    <title><?php echo get_bloginfo('name'); ?></title>
    <meta name="description" content="<?php echo get_bloginfo('description'); ?>">

    <? if (is_single()) {
        $pieces = get_extended($post->post_content);
        $intro = strip_shortcodes($pieces['extended']);
        ?>
    <!-- FB -->
    <meta property="og:title" content="<? echo the_title(); ?>"/>
    <meta property="og:description" content="<? echo trim(strip_tags(str_replace(["\n", "\r"], "", $intro))); ?>"/>
    <meta property="og:url" content="<? echo get_permalink(); ?>"/>
    <meta property="og:image" content="<? echo has_post_thumbnail() ? get_site_url() . get_the_post_thumbnail_url() : '/wp-content/themes/mxworld/htmlassets/no-banner.png'; ?>"/>
    <? } ?>

    <link rel="icon" href="/wp-content/themes/mxworld/htmlassets/favicon.ico">

</head>
<body>

<div id="fb-root"></div>
<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/nl_NL/all.js#xfbml=1";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

<div class="container">
    <div class="row header">
        <div class="space-logo"></div>
        </a>
        <div class="banner-container">
            <?php echo adrotate_group(50); ?>
        </div>
        <h1 class="hide-mobile"><a href="<?php echo get_bloginfo('wpurl'); ?>"><img class="logo" src="<?php echo get_bloginfo('template_directory'); ?>/htmlassets/logo-mxworld.png" alt="MX World"></a></h1>
        <h1 class="show-mobile"><a href="<?php echo get_bloginfo('wpurl'); ?>"><img src="<?php echo get_bloginfo('template_directory'); ?>/htmlassets/logo-mxworld-mobile.png" alt="MX World"></a></h1>
    </div>
    <div class="row">
        <div class="space-logo"></div>
        <form role="search" method="get" id="searchform" class="searchform" action="<?php echo get_bloginfo('wpurl'); ?>">
            <div class="search-container">
                <input type="text" class="search" value="" name="s" id="s" placeholder="Search...">
                <input type="submit" id="searchsubmit" class="header_search_submit" value="Search">
            </div>


            <div>

            </div>

    </div>