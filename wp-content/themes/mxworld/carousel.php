<?php
$args = array(
    'numberposts' => 10,
    'offset' => 0,
    'category' => 0,
    'orderby' => 'post_date',
    'order' => 'DESC',
    'include' => '',
    'exclude' => '',
    'meta_key' => '',
    'meta_value' => '',
    'post_type' => 'post',
    'post_status' => 'publish',
    'suppress_filters' => true
);

$recent_posts = wp_get_recent_posts($args, ARRAY_A);
//print_r($recent_posts);
?>

<div class="row">
    <div id="Carousel" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <?
            foreach ($recent_posts as $key => $post) { ?>
                <li data-target="#Carousel" data-slide-to="<? echo $key; ?>"
                    class="<? if ($first_nav == 0) echo 'active'; ?>"></li>
                <?
            }
            ?>
        </ol>

        <div class="carousel-inner" role="listbox">
            <?
            foreach ($recent_posts as $key => $post) {
                $url = has_post_thumbnail($post['ID']) ? get_the_post_thumbnail_url($post['ID']) : '/wp-content/themes/mxworld/htmlassets/no-banner.png';
                $pieces = get_extended($post['post_content']);
                // Split intro if too long
                $pieces['main'] = strip_shortcodes(strip_tags($pieces['main']));
                $intro = strlen($pieces['main']) > 250 ? substr($pieces['main'], 0, 250).'..' : $pieces['main'];
                ?>
                <div class="carousel-item <? if ($key == 0) echo 'active'; ?>">
                    <img class="d-block" src="<? echo $url; ?>" alt="First slide">
                    <div class="carousel-caption d-xs-block text-right">
                        <h2><?php echo $post['post_name']; ?></h2>
                        <p class="title-text"><?php echo $intro; ?></p>
                        <a class="read-more" href="<? echo esc_url(get_post_permalink($post['ID'])); ?>">Lees meer <i class="fas fa-angle-double-right"></i></a>
                    </div>
                </div>
                <?
            }
            ?>
        </div>

        <a class="carousel-control-prev" href="#Carousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#Carousel" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>