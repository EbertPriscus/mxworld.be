<div class="row">
    <div class="col-sm-12 footer">
        <img src="<?php echo get_bloginfo('template_directory'); ?>/htmlassets/logo-mxworld-white.png" alt="MX World"><br>
        Copyright 2017 mxworld.be<br>
        <a href="mailto:marcel@mxworld.be">Contacteer ons</a>
    </div>

</div>
</div>

<script>
    $(document).ready(function() {
        $("#lightgallery").lightGallery({
            subHtmlSelectorRelative: true,
            download: false
        });
    });
</script>

</body>
</html>
